# Mixer & Chorus

## Mixer

The panning of all 6 channels is fixed. There's no adjustment at all at this stage.

![Mixer](pics/mixer.png)*Mixer and output*

AUX_L and AUX_R allow mixing in external sound sources.

![Auxilliary input](pics/auxin.png)*Auxilliary input*

## Chorus

There is also an adjustable delay built using a bucket brigade chip that implements a noisy low-fi chorus effect. 

0-5V CV that adjusts delay line frequency is mapped to ±8V range that controls a VCO.
![Delay CV](pics/delay-cv.png)*Delay CV Mapper*

The VCO is built using a 555. It is adjustable within approximately 50-100KHz. The exact number may be slightly off, I did not really verify the frequency. This circuit, like most of the chorus, is borrowed from *Maestro* synth.

![Delay VCO](pics/delay-vco.png)*Should have used a 555*

An oscillator built using a 555 generates frequency around 50-100kHz, which is split into two phases needed for the BBD chip using U34A. The output frequencies should be around 25-50kHz. 

Bucket brigade chips pass analog charges, the rate they do it at is akin to digital sampling rate. Just like a digital delay with ADC and DAC, a BBD-based delay line also needs an antialiasing and reconstructing filter to avoid introducing significant artefacts.

![Antialiasing filter](pics/aa-filter.png)*Antialiasing filter before the bucket brigade delay line*

Copying this filter design was probably a mistake. It seems to introduce considerable noise. It could be something else, but everything points to the filters. The frequency response of this filter is approximately this:

![AA filter freqz](pics/aa-filter-freqz.png)*Antialiasing filter frequency response*

Another important function of this circuit is to set DC offset of the signal at the input of BBD chip.

The delay line itself is built using КА528БР2 BBD chip. They are somewhat notorious for being noisy, and the one used here gets a lot of bad rap. I cannot really compare this particular chip to others as I have never had a BBD-based effect before. From my personal point of view it performs as expected.

![BBD+Reconstructing filter+Level](pics/bbd-aa-level.png)*Delay, reconstructing filter and level adjust*

Only one half of the delay chip is used. I'm not sure if using both halves is worth it. It is possible to use the second half as well and there is one extra control voltage. This is something for later on.

After delay, the signal is reconstructed on a filter identical to the one before it and attenuated on a single transistor VCA Q15-U36A. Delayed signal is then mixed in to the L/R channels of the mixer. The delayed signal is monophonic, mixed with stereo main signal it sounds pretty interesting. I would say it makes the original more stereo. By varying mix level, frequency and frequency change rate, some interesting sounds can be produced.

## Le Pétoscope

I think it's very useful and interesting to monitor the waveform while debugging the synth or just playing with it. Of course it can be done using an oscilloscope or using a DAW plugin. But it's so much cooler to have a tiny little oscilloscope piggybacked on top of your synth. 

![Le Pétoscope](pics/lepetoscope.jpg)*Le Pétoscope*

While waiting for revision B of the boards, I had plenty of time and conveniently a bunch of unused parts like OLED displays and ESP12F modules, which have a rather fast builtin ADC. This is a separate project, linked [here](https://gitlab.com/svofski/lepetoscope/).

