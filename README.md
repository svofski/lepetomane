Le Pétomane
===========
Le Pétomane is a hybrid digital-analog polyphonic (6-voice) synthesizer. The main tones are produced using 2x 82c54 PIT chips that generate base frequencies used to create sawtooth, triangle, and PWM waves. Each tone is passed through its individual Polivoks filter. Channels are attenuated,  mixed together and passed through an adjustable КА528БР2 BBD delay line.

![Le petomane](pics/pic1.jpg)*Le Pétomane*

Most of the processing is done in analog. Control voltages are generated on a single R2R DAC and multiplexed to a bank of sample & hold circuits.

The interface is designed with a retro home computer connectivity in mind: it can be connected to the host computer via a 24-bit parallel I/O interface. Currently though the brain of the synthesizer is a Teensy++ 2.0 board that receives MIDI messages and translates them into write signals to the timers, DAC and configuration registers.

The synth uses some "modern" SMD chips as board space and cost-saving measures. The spirit of the design however is to be as close to the mid-1980s era as possible where it counts. 

Detailed description can be found in this little book:
[Le Pétomane Book](https://svofski.gitlab.io/lepetomane/)

Demos
=====

* [Weird Dreams by David Whittaker, sequenced by Oedipus](https://www.youtube.com/watch?v=OYkpsddoruU)
* [Doctor Who Theme by Ron Grainer as imagined by Delia Derbyshire](https://www.youtube.com/watch?v=7-2ZBxtD-zc)

Project Status
==============

- [x] individual circuits prototyping
- [x] schematic/pcb design [PDF](hw/kicad/lepetomane/doc/schematic/lepetomane-b.pdf)
- [x] pcb production
- [x] teensy board adapter
- [x] main board
- [x] waveshaper/filter boards rev.a -- after way too many debug wires PCB rev.B is ready, waiting for delivery
- [x] waveshaper/filter rev.B in hardware
- [x] mixer/chorus board -- fixes documented in schematic
- [x] midi/hw driver firmware 80%
- [ ] demos
- [ ] ctrlr panel - 80% - not sure if vst version is worth it yet
