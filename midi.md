# MIDI Implementation

Le Pétomane is a MIDI synthesizer. It accepts regular messages on channels 1-6. In order to make instrument setup easier from a DAW, it can also accept instrument setup messages on channels up to 16 to make it easier to configure instruments from a DAW.

There are 16 instruments that can be selected using program change message. All parameters are configured using NRPN messages. To set a parameter for the current instrument on channel 1, send corresponding NRPN on channel 1.

## NRPN Messages

```
0   0-3     waveform select: 0=saw, 1=square, 2=triangle (2,4,6), 3=off
1   0-1     ringmod on/off
2   0-1     noise on/off
3   0-1     filter band lowpass/bandpass
4   0-255   filter cutoff
5   0-255   filter resonance
6   0-255   pulse width for channels (2,4,6)
7   0-200   pitch adjust 0 = -100 cents, 100 = 0, 200 = +100
8   0-60    filter cutoff keyboard tracking, 20 is reasonable
9   0-4     pitch mod scale 1..5, multiplies pitch mods by this value
10  0-6     LFO target: 0=off, 1=pitch, 2=fc, 3=fq, 4=pulse width, 5=volume, 6=waveform
11  0-1     LFO retrigger on note on

12  0-255   portamento 0=off, 1=slow, 255=fast

16 0-255   volume envelope pre-delay
17 0-255   volume envelope attack
18 0-255   volume envelope decay
19 0-255   volume envelope sustain
20 0-255   volume envelope release
21 0-255   volume envelope amount
22 0-255   volume envelope velocity

32  0-255   filter cutoff envelope pre-delay
33  0-255   filter cutoff envelope attack
34  0-255   filter cutoff envelope decay
35  0-255   filter cutoff envelope sustain
36  0-255   filter cutoff envelope release
37  0-255   filter cutoff envelope amount
38  0-255   filter cutoff envelope velocity
39  0-1     filter cutoff envelope sign 0=-, 1=+

48  0-255   filter resonance envelope pre-delay
49  0-255   filter resonance envelope attack
50  0-255   filter resonance envelope decay
51  0-255   filter resonance envelope sustain
52  0-255   filter resonance envelope release
53  0-255   filter resonance envelope amount
54  0-255   filter resonance envelope velocity
55  0-1     filter resonance envelope sign 0=-, 1=+

64  0-255   pulse width envelope pre-delay
65  0-255   pulse width envelope attack
66  0-255   pulse width envelope decay
67  0-255   pulse width envelope sustain
68  0-255   pulse width envelope release
69  0-255   pulse width envelope amount
70  0-255   pulse width envelope velocity
71  0-1     pulse width envelope sign 0=-, 1=+

80  0-255   pitch envelope pre-delay
81  0-255   pitch envelope attack
82  0-255   pitch envelope decay
83  0-255   pitch envelope sustain
84  0-255   pitch envelope release
85  0-255   pitch envelope amount
86  0-255   pitch envelope velocity
87  0-1     pitch envelope sign 0=-, 1=+

96  0-255   LFO pre-delay
97  0-3     LFO shape: 0=triangle, 1=saw, 2=square, 3=random s&h
98  0-511   LFO amount: 0=-255, 255=0, 511=+256
99  0-255   LFO offset: 0=-128, 128=0, 255=127
100 0-16383 LFO rate
101 0-1     LFO retrigger
102 0-255   LFO phase
```

Parameters 0, 1, 2, 3, 8, 9, 10, 11, 39, 55, 71, 87, 97 are expected
to be in short NRPN format (MSB only). Others are normal 14-bit NRPNs.

## Chorus control

Chorus is global. It is controlled via CC messages on channel 5. The chorus LFO
always has triangular shape.

```
91  0-127   chorus rate
93  0-127   chorus mix level
94  0-127   chorus LFO amount -64..63
95  0-127   chorus LFO speed
```

