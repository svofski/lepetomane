# Waveshaper & Filter

Each of waveshaper & filter boards implements 2 voices. The complete synthesizer has 3 such boards comprising 6 independent voices.

![Waveshaper&Filter Board](pics/board-waveshaper-filter.jpg)*Waveshaper & Builder Rev.B Fully Assembled*

## Waveshaper

### Channel 1

At the input we have 2 square frequencies, but we would like more tonal diversity. The first channel can have a square or sawtooth waveshape.

![saw and square](pics/ch0-saw-square.png)*saw and square circuit*

FSQ0 is the tone frequency times 8. It is fed to a binary counter U17B, and its output together with the lowest significant bit from U27D serves as input to a 4-bit R2R DAC that has a semblance of sawtooth on its output. With only 16 steps it's rather steppy, but it's a saw nevertheless. There will be a basic RC filter to take the edge off it and a full-featured filter to smooth it out to taste.

### Channel 2

It starts out the same as Channel 1, but there are a couple of tricks that make it more advanced. There are two additional control bits: `TRIA1` (triangle) and `RING1` (ringmod).

![ch2 with ringmod](pics/ch1-triangle-ringmod.png)*Channel 2 can do Ringmod, Triangle and PWM*

There is an additional block of 2XOR elements (U28 A/B/C/D) between the counter output and DAC in this channel. When `RING1` bit is on, it passes the square wave from previous channel (Channel 1) and we get a rough multiplication of two channels which produces inharmonic frequencies characteristical of ring modulator. When a `TRIA1` bit is on, the output is inverted every other counter overflow, which gives us a triangle wave at 1/2 frequency. 

This output is then compared to `CVP1` control voltage on U39B, which gives us control over pulse width, or PWM. Because the DAC has such low resolution, we only get 16 different pulse widths.

### Noise source

There's an independent analog noise source on every board. Noise can be enabled for channels 2, 4, 5, 6. It's a pretty decent white noise source on reverse biased pn junction, amplified and centered around 2.5V level for convenient consumption.

![noise source](pics/noisenik.png)*White noise source*

### Waveform selector

At this stage we select which of the variety of available waveforms will be passed on. It is also possible to mix in noise source.

![waveform selector](pics/wavesel.png)*Waveform selector*

Control bits `WAVESEL0` and `WAVESEL1` pick which of the waveforms will be passed further on for each channel. As detailed above, Channel 1 can have sawtooth or square shape, Channel 2 can have sawtooth/triangle, or PWM shape, optionally ring-modulated with Channel 1. Control bits `NOISE0` and `NOISE1` add white noise to the mix. A voice can be configured to be noise-only by stopping its frequency source.

*It is worth noting that up to this point all analog signals are kept within 5V logic range, which allows powering analog switches from 0-5V. It makes it possible to select analog signals using TTL logic levels.*

## Volume Envelope

In order to minimise parts count and costs, Le Pétomane uses a very basic VCA also known as "a single transistor VCA".

![VCA](pics/vca.png)*A Single-Transistor VCA*

This VCA works surprisingly well, but it has some strict requirements for its input signals. Because it must have input signal above zero, and because it's difficult to be sure in what kind of voltages are at the output of resonant filters, in Le Pétomane volume envelope is applied before the filters and not after.

How such VCA works is beautifully explained in this short [video](https://www.youtube.com/watch?v=abUMAo_ODv0) by *The Audiophool*.

## Filter

Each channel of Le Pétomane has a separate independently controlled resonant filter based on Polivoks by Vladimir Kuzmin. The filter cutoff is controlled by programming PGA, which is done by adjusting a current at its programming pin. To adjust resonance, we need a control current for the transconductance amplifier. This part is inspired by the Shruthi-1 Polivoks clone by Mutable Instruments.

![filter control currents](pics/filtercontrol.png)*Filter control currents*

The true gem of Le Pétomane is of course the legendary Polivoks filter.

![ПОЛИВОКС](pics/polivoks.png)*ПОЛИВОКС*

When the input and control signals are kept in range, it performs beautifully. I used the original 140УД1208 chips, but could not do without LM13700. It's a bit ironic because I heard that one of the reasons why Vladimir Kuzmin invented this fantasic design was unavailability of transconductance amplifier chips at the time. Anyway today it's just a convenient utility. LM13700 has two gates, so one chip works for resonance control for two channels.

## Filter Select

Finally, we get to select which of the filter bands to use: Lowpass or Bandpass. This is done using good old CD4053.

![Band Select](pics/filterselect.png)*Filter Band Select*

There is an important detail here. Unlike previous stages where a similar circuit was used, the output of the filter is all over the place — it may be centered around zero, or may swing pretty far off centre. Using logic levels for the analog switch wouldn't work well in this case. However we also cannot use ±12V to power a CD4053, as its analog voltage range must be not more than 18V. Fortunately, the negative voltage is only needed to bias the MOSFETs. I chose to add a 10K/10K voltage divider to bias VEE at -6V, which works fine. It would probably be better to use higher values of resistance to reduce the current.

The logic signals `FSEL0` and `FSEL1` are offset into 12V range by Q7 and Q8.

At the outputs `CHAN0` and `CHAN1` we have two real analog voices. They only need to be mixed together with the rest of the choir, maybe delayed a little bit on a bucket brigade chip, and sent to LINE OUT. This happens in the [Mixer & Chorus](chorus.md).




