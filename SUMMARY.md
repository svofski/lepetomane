# Summary

* [About Le Pétomane](README.md)

# What it's made of

* [Main Logic Board](logic-board.md)
* [Waveshaper & Filter](filters.md)
* [Mixer & Chorus](chorus.md)
* [Control Signals](control.md)
* [MIDI Implementation](midi.md)
