Le Pétomane
===========


Control
-------


Parallel port like PU on v06c, divided in 3 8-bit ports PA, PB, PC.

### Port A

```
7 6 5 4 3 2 1 0
| | | | | | |.|.. internal reg no
| | | | | |
| | | | |.|...... device select: 0: PPI, 1: TIMER0, 2: TIMER1, 3: ?
| | | |
| |.|.|.......... RING 4x5, 2x3, 1x0
|
|................ WRn 0 = write data to selected device / register
```

### Port B

Data

### Port C

```
7 6 5 4 3 2 1 0
| | | |.|.|.|.|... analog mux channel select 0..23
| | |
|.|.|............. TRIA5, TRIA3, TRIA1 1 = triangle half freq, 0 = saw
```

### PPI.CW (PA[3:0] == 0011)

8255 control word (0x80 = mode 0, all output)

### PPI.A (PA[3:0] = 0000)

8-bit DAC

### PPI.B (PA[3:0] = 0001)

```
7 6 5 4 3 2 1 0
| | |.|.|.|.|.|... WAVESEL5..0 waveform select 0 = saw, 1 = square
| |
|.|............... NOISE5..4 mix in white noise on channels 5, 4
```

### PPI.C (PA[3:0] = 0010)

```
7 6 5 4 3 2 1 0
| | |.|.|.|.|.|... FSEL5..0  filter select 0 = lowpass, 1 = bandpass
| |
|.|............... NOISE3,NOISE1
```

### TIMER0 PA[3:0] = 01xx

### TIMER1 PA[3:0] = 10xx

### ALL OFF PA[3:0] = 11xx


### AMUX Channels (CV numbers)
```
0  1  2         Chan 0: ENV F Q
3  4  5   6     Chan 1: ENV F Q PWM
7  16 17        Chan 2: ENV F Q
18 19 20  21    Chan 3: ENV F Q PWM
22 23 8         Chan 4: ENV F Q
9  10 11  12    Chan 5: ENV F Q PWM
13 14           Delay 10..20ms, Delay volume 
15              MYSTERY?
```

Play a 440 Hz square tone on channel 0
======================================

1. Timer 0, channel 0: set mode and frequency

Set timer 0 CW address:
```PA = 1xxx 01 11: WRn=1, device = 1, register = 3```

Set timer 0 CW value
```
PB = 00 11 011 0
        |  |   |   \_  no bcd
        |  |    \____  mode 3 square wave gen
        |   \________  16-bit write LSB, then MSB
         \___________  counter 0
```

Perform write: 
```
PA ^= 0x80;
PA ^= 0x80;
```

Counter value = 8000000 / 440 / 16 = 8000000/440/16 = 1136, 0x470

```
PA = 0x84; // WRn=1, device = 1, register = 0: counter value
PB = 0x70; // LSB
PA ^= 0x80;
PA ^= 0x80;
PB = 0x04; // MSB, address does not change
PA ^= 0x80;
PA ^= 0x80;
```

2. Set waveform to square: WAVESEL0=0

```
PA = 0x81; // address of PPI.B: device 0 register 1
PB = 0x00; // value
PA ^= 0x80; PA ^= 0x80; // write strobe
```

3. Set filter to LP: FSEL0=0

```
PA = 0x82; // address of PPI.C: device 0 register 2
PB = 0x00; // value, all 0
PA ^= 0x80; PA ^= 0x80; // write strobe
```

4. Set filter CV to max open: CVF0 = 255, CQ to min Q = 0

```
PC = 0x1f;  // divert AMUX
PA = 0x80;  // address of PPI.A: device 0 register 0
PB = 0xff;  // DAC = 255
PA ^= 0x80; PA ^= 0x80; // write strobe

PC = 1;    // CVF0 is AMUX.1
udelay(SHDELAY);    // wait for S&H transient
PC = 0x1f;  // divert AMUX

PB = 0x00;  // DAC = 0
PA ^= 0x80; PA ^= 0x80; // write strobe

PC = 2;     // CVQ0 is AMUX.2
udelay(SHDELAY);
PC = 0x1f;  // divert AMUX
```

5. Set envelope to half: CVE0 = 127

```
PC = 0x1f;  // divert AMUX
PA = 0x80;  // select DAC
PB = 0x7f;  // DAC = 127
PA ^= 0x80; PA ^= 0x80; // write strobe
udelay(SHDELAY);
PC = 0x1f;
```

