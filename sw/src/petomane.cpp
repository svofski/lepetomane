#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>
#include "petomane.h"

#define WR_PU_A(x)  { PORTD=x; }
#define WR_PU_B(x)  { PORTC=x; }
#define WR_PU_C(x)  { PORTF=x; }

#define DDR_PU_A    DDRD
#define DDR_PU_B    DDRC
#define DDR_PU_C    DDRF


// assuming U4A output impedance 200 ohm, 5V set voltage and s&h cap 0.1uF
// time constant RC is 20us, 50us should cover all bases
#define SAMPLE_TIME_US 50 // 30 is too short for full swing

#define AMUX_MASK   0x1f    // bits 4..0 of PU.C select analog mux
#define ADDR_MASK   0x0f    // bits 3..0 of PU.A select device addr

// pull WRn low momentarily
void wrn_strobe()
{
    PORTD ^= 0x80;
    //_delay_us(1);
    PORTD ^= 0x80;
}

void Petomane::bsr_dev(uint8_t device, uint8_t reg, uint8_t mask, uint8_t set)
{
    _regs.pa = 0x80 | (_regs.pa & 0xf0) | (device << 2) | reg;
    WR_PU_A(_regs.pa);  // set address A = 1 x x x d d r r
    _regs.shadow[device].r[reg] &= mask;
    _regs.shadow[device].r[reg] |= set;
    WR_PU_B(_regs.shadow[device].r[reg]);
    wrn_strobe();
}

void Petomane::bsr_pu(uint8_t port, uint8_t mask, uint8_t set)
{
    switch (port) {
        case PU_A:
            _regs.pa = (_regs.pa & mask) | set;
            WR_PU_A(_regs.pa);
            break;
        case PU_B:
            _regs.pb = (_regs.pb & mask) | set;
            WR_PU_B(_regs.pb);
            break;
        case PU_C:
            _regs.pc = (_regs.pc & mask) | set;
            WR_PU_C(_regs.pc);
            break;
    }
}


Petomane::Petomane()
{
    // PF7..0 == pins F7..0 == arduino 38..45   PU.A
    // PC7..0 == pins C7..0 == arduino 10..17   PU.B
    // PB7..0 == pins B7..0 == arduino 20..27   PU.C
    // set direction here
    DDR_PU_A = 0xff;
    DDR_PU_B = 0xff;
    DDR_PU_C = 0xff;
}

void Petomane::board_init()
{
    _regs = {};

    _regs.pa = 0x80;
    _regs.pc = AMUX_MASK; // AMUX = 31
    WR_PU_A(_regs.pa);
    WR_PU_B(_regs.pb);
    WR_PU_C(_regs.pc);

    // init 8255 to mode 0, all output
    bsr_dev(DEV_PPI, PPI_CW, 0, 0x80);

    // stop timers
    for (int i = 0; i < 6; ++i) {
        set_enabled(i, false);
    }

    // set all CV to 0
    for (int i = 0; i < 24; ++i) {
        set_cv(i, 0);
    }
}

void Petomane::set_enabled(Channel chan, bool enable)
{
    if (chan < 6) {
        error = ERR_OK;

        if (chan < 3) {
            uint8_t cw = (chan << 6) | (3 << 4) | ((enable ? 3 : 1) << 1);
            bsr_dev(DEV_CTR0, CTR_CW, 0, cw);
        }
        else {
            uint8_t cw = ((chan - 3) << 6) | (3 << 4) | ((enable ? 3 : 1) << 1);
            bsr_dev(DEV_CTR1, CTR_CW, 0, cw);
        }
        wrn_strobe();
    }
    else {
        error = ERR_CHAN;
    }
}

void Petomane::set_div(Channel chan, uint16_t divider)
{
    uint8_t dev, reg;
    if (chan < 3) {
        dev = DEV_CTR0;
        reg = chan;
    }
    else {
        dev = DEV_CTR1;
        reg = chan - 3;
    }
    bsr_dev(dev, reg, 0, divider & 0xff);
    bsr_dev(dev, reg, 0, divider >> 8);
}

void Petomane::set_waveform(Channel chan, Waveform wf)
{
    error = ERR_OK;

    Waveform main = (Waveform)(wf & WF_MASK);
    if (main == WF_SQUARE || main == WF_SAW) {
        // TRIANGLE.chan = 0
        if (chan & 1) {
            bsr_pu(PU_C, ~(0x20 << (chan >> 1)), 0);
        }

        if (main == WF_SAW) {
            // SAW: WAVESEL.chan = 0
            bsr_dev(DEV_PPI, PPI_B, ~(1 << chan), 0);
        }
        else {
            // SQUARE: WAVESEL.chan = 1
            bsr_dev(DEV_PPI, PPI_B, 0xff, 1 << chan);
        }
    }
    else if (main == WF_TRIANGLE && (chan & 1)) {
        // TRIANGLE only works on channels 1,3,5
        // TRIANGLE.chan = 1
        bsr_pu(PU_C, 0xff, 0x20 << (chan >> 1));
        // SAW: WAVESEL.chan = 0
        bsr_dev(DEV_PPI, PPI_B, ~(1 << chan), 0); 
    }
    else {
        error = ERR_WAVEFORM;
    }

    set_ringmod(chan, wf & WF_RINGMOD);
    set_noise(chan, wf & WF_NOISE);
}

void Petomane::set_filter_band(Channel chan, Filter b)
{
    error = ERR_OK;

//    Serial.printf("## set filter band %d = %02x\n",
//            chan, 1 << chan);

    if (b == FILTER_BANDPASS) {
        // FSEL.chan = 0
        bsr_dev(DEV_PPI, PPI_C, ~(1 << chan), 0);
    }
    else {
        // FSEL.chan = 1, FILTER_BANDPASS
        bsr_dev(DEV_PPI, PPI_C, 0xff, 1 << chan);
    }
}

void Petomane::set_ringmod(Channel chan, bool on)
{
    error = ERR_OK;
    if (chan & 1) {
        uint8_t bit = 0x10 << (chan >> 1);
        bsr_pu(PU_A, ~bit, on ? bit : 0);
    }
    else {
        error = ERR_RINGMOD_CHAN;
    }
}

void Petomane::set_noise(Channel chan, bool on)
{
    error = ERR_OK;

    if (chan == 1) {
        uint8_t bit = 1 << 6;
        bsr_dev(DEV_PPI, PPI_C, ~bit, on ? bit : 0); 
    }
    else if (chan == 3) {
        uint8_t bit = 1 << 7;
        bsr_dev(DEV_PPI, PPI_C, ~bit, on ? bit : 0); 
    }
    else if (chan == 4 || chan == 5) {
        uint8_t bit = 1 << (chan + 2);
        bsr_dev(DEV_PPI, PPI_B, ~bit, on ? bit : 0);
    }
}

void Petomane::set_cv(uint8_t cvn, uint8_t value)
{
    if (cvn < 24) {
        error = ERR_OK;

        // DAC is on PPI reg A, write value
        bsr_dev(DEV_PPI, PPI_A, 0, value);
        // AMUX to CV channel
        bsr_pu(PU_C, ~AMUX_MASK, cvn);
        // wait until the sample capacitor is charged
        _delay_us(SAMPLE_TIME_US);
    }
    else {
        error = ERR_AMUX_CHAN;
    }

    // divert analog mux to nonexistent chan 31
    bsr_pu(PU_C, ~AMUX_MASK, AMUX_MASK);
}

void Petomane::cv_env(Channel chan, uint8_t value)
{
    static const uint8_t cvmap[] = {0, 3, 7, 18, 22, 9};

    if (chan < 6) {
        error = ERR_OK;
        set_cv(cvmap[chan], value);
    }
    else {
        error = ERR_CHAN;
    }
}

void Petomane::cv_cutoff(Channel chan, uint8_t fc)
{
    static const uint8_t fmap[] = {1, 4, 16, 19, 23, 10};

    if (chan < 6) {
        error = ERR_OK;
        set_cv(fmap[chan], fc);
    }
    else {
        error = ERR_CHAN;
    }
}

void Petomane::cv_resonance(Channel chan, uint8_t q)
{
    static const uint8_t qmap[] = {2, 5, 17, 20, 8, 11};

    if (chan < 6) {
        error = ERR_OK;
        set_cv(qmap[chan], q);
    }
    else {
        error = ERR_CHAN;
    }
}

void Petomane::cv_pwm(Channel chan, uint8_t val)
{
    static const uint8_t cvmap[] = {31, 6, 31, 21, 31, 12};
    if (chan < 6) {
        uint8_t cvn = cvmap[chan];
        if (cvn != 31) {
            set_cv(cvn, val);
        }
        else {
            error = ERR_PWM_CHAN;
        }
    }
    else {
        error = ERR_CHAN;
    }
}

void Petomane::cv_chorus_rate(uint8_t rate)
{
    set_cv(13, rate);
}

void Petomane::cv_chorus_level(uint8_t level)
{
    set_cv(14, level);
}

// CVX0 = 13, CVX1 = 14, CVX2 = 15
