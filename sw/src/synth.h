#ifdef UNITY
#include <stdio.h>

#ifndef TRACE_ENV
#define TRACE_ENV 0
#endif


#ifdef ARDUINO
#define printf Serial.printf
#else
#include <stdlib.h>
#endif

#endif


#ifdef ARDUINO
#define CHATTY_PARAMS 0 // no Serial.print() on native
#define CHATTY_SET_PARAM 0
#endif


#include <stdint.h>
#include "fp.h"
#include "synth_types.h"
#include "petomane.h"
#include "tuning.h"
#include "onnote.h"

#define MIN(a,b) (((a)<(b)) ? (a) : (b))
#define MAX(a,b) (((a)>(b)) ? (a) : (b))

/*

6 independent monophonic channels

Initial state:
    OSC SAW/SQUARE/TRIANGLE/PWM
    Volume
    Filter LP/BP
    Filter tracking

    Noise delay, noise on time, noise off time (repeats)
    Ringmod delay, on time, off time (repeat)

    PWM duty    + ADSR + LFO + LFO Delay
    Volume      + ADSR + LFO + LFO Delay
    F           + ADSR + LFO + LFO Delay
    Q


Volume envelope:
    A = 1/50s counts from 0 to Volume
    D = 1/50s counts from Volume to S
    S = sustain level
    R = 1/50s counts to 0

Filter/tracking etc..

    baseline F/Q -- specify for which note?
    tracking = F is halved/doubled each octave..

    F ADSR -
        A = number of ticks to go from 0 to F, 0 = instant F
        D = ticks from F to S
        S = sustain level
        R = on release
    Q

    PWM ADSR - same as F

    LFO...


*/

enum voiceflags_ : uint16_t {
    F_ZERO                      = 0,
    F_WF_CHANGED                = 1 << 0,
    F_PITCH_CHANGED             = 1 << 1,
    F_VOL_CHANGED               = 1 << 2,
    F_FCUT_CHANGED              = 1 << 3,
    F_FRES_CHANGED              = 1 << 4,
    F_PW_CHANGED                = 1 << 5,
    F_FILTER_CHANGED            = 1 << 6, //  LP/BP
    F_NOISE_CHANGED             = 1 << 7, // noise on/off
    F_NOTE_STARTED              = 1 << 8,
    F_RINGMOD_CHANGED           = 1 << 9,
    F_LFO_CHANGED               = 1 << 10,
};

typedef uint16_t voiceflags;


static inline uint8_t clamp8u(int16_t x) {
    return x < 0 ? 0 : x > 255 ? 255 : x;
}

static inline int16_t clamp16i(int16_t x, int16_t min, int16_t max)
{
    return x < min ? min : x > max ? max : x;
}

// attack rate, decay rate, sustained value, release rate
struct ADSR {
    uint8_t delay;
    uint8_t a, d, s, r;
    uint8_t amount;     // 0..255
    uint8_t tracking;   // velocity tracking: 0 = don't care, 255 = full
    int8_t  sign;       // +- where applicable

    bool set_param(uint8_t index, uint8_t value)
    {
        if (index < sizeof(ADSR)) {
            if (index == 7) {
                value = value == 1 ? 1 : 255;
            }
            ((uint8_t *)this)[index] = value;
#if CHATTY_PARAMS
            Serial.print(adsr_param_fstr(index));
            Serial.print("=");
            Serial.println(value);
#endif
            return true;
        }

        return false;
    }

    const bool operator==(const ADSR& rhs) const {
        return delay == rhs.delay &&
            a == rhs.a &&
            d == rhs.d &&
            s == rhs.s &&
            r == rhs.r &&
            amount == rhs.amount &&
            tracking == rhs.tracking &&
            sign == rhs.sign;
    }

    bool operator!=(const ADSR& rhs) const {
        return !(*this == rhs);
    }
};

struct LFOParams {
    uint16_t delay;         // pre-delay
    uint8_t shape;          // LFO_Shape
    int16_t amount;         // +/- 0 = -255 255 = 0 511 = +256
    int16_t offset;         // 1/2 amount for centered
    uint16_t rate;          // @ 200Hz
                            // 200 * 1/16383 = 0.012 Hz
                            // 200 * 1 = 200 Hz
    uint8_t phase;          // 255 = 360
    uint8_t retrigger;      // 0 = keep rolling

    bool set_param(uint8_t index, uint16_t value)
    {
#if CHATTY_PARAMS
        Serial.print(lfo_param_fstr(index));
        Serial.print(" = ");
        Serial.print(value);
#endif

        switch (index) {
            case 0:
                delay = value;
                break;
            case 1:
                shape = value;
                if (shape > LFO_LAST) {
                    shape = value = LFO_TRIANGLE;
                }
#if CHATTY_PARAMS
                Serial.print(' ');
                Serial.print(lfo_shape_fstr(shape));
#endif
                break;
            case 2:
                amount = ((int16_t)value) - 255;
                break;
            case 3:
                offset = ((int16_t)value) - 128;
                break;
            case 4:
                rate = value; // 0..16383
                break;
            case 5:
                retrigger = value;
                break;
            case 6:
                phase = clamp8u(value);
                break;
            default:
                return false;
        }

#if CHATTY_PARAMS
        Serial.println();
#endif

        return true;
    }

    bool operator==(const LFOParams& rhs) const {
        return delay == rhs.delay &&
            shape == rhs.shape &&
            amount == rhs.amount &&
            offset == rhs.offset &&
            rate == rhs.rate &&
            retrigger == rhs.retrigger;
    }

    bool operator!=(const LFOParams& rhs) const {
        return !(*this == rhs);
    }
};

// initial delay, periodic waveform
struct LFO {
    uint8_t shape;
    uint8_t flags;
    uint16_t delay;
    int16_t amount;
    int16_t offset;
    uint8_t phase0; // initial phase

    uint32_t phase;
    uint32_t delta;
    uint16_t delay_ctr;

    int16_t output;

    void init(const LFOParams & p)
    {
        shape = p.shape;
        delay = p.delay;
        amount = p.amount;
        offset = p.offset;
        phase0 = p.phase;
        flags = p.retrigger ? LFO_RETRIGGER : 0;

        // 200hz / (0xffffffff/(16383<<15))
        // 0.0015..24.99 Hz
        delta = ((uint32_t)p.rate) << 15;
    }

    void begin(uint8_t velocity)
    {
        if ((flags & LFO_RETRIGGER) || (flags & LFO_ON) == 0) {
            output = 0;
            phase = (uint32_t)phase0 << 24;
            delay_ctr = delay;
        }
    }

    void release(uint8_t velocity)
    {
        flags &= ~LFO_ON;
    }

    // LFO update tick
    bool update_tick()
    {
        if (delay_ctr) {
            --delay_ctr;
            return false;
        }

        if (amount == 0 && output == 0 && delta == 0)
            return false;

        int16_t prev = output;

        int16_t x = phase >> 24; // integral part of phase
        phase += delta;

        uint16_t mul;

        switch (shape) {
            case LFO_TRIANGLE:  // 0
                output = (x < 128) ?  x : (256 - x);
                output *= 1 * amount;
                output >>= 7;
                break;
            case LFO_SAW:       // 1
                mul = (uint8_t)x * (uint16_t)abs(amount);
                output = mul >> 8;
                if (amount < 0) {
                    output = -output;
                }
                break;
            case LFO_SQUARE:    // 2
                output = (x < 128) ? 0 : amount;
                break;
            case LFO_RANDOMSH:  // 3
                if (phase + delta < phase) {
#ifdef ARDUINO
                    output = random(0, abs(amount));
                    if (amount < 0) output = -output;
#else
                    output = (amount + 1) * (rand()>>16) >> 16;
#endif
                }
                break;
            default:
                break;
        }

        output += offset;

        return output != prev;
    }
};


// for realtime processing
struct Envelope {
    State state;
    int16_t output;     // can output negative values

    U_FP current;

    U_FP a_top;         // when value >= a_top, attack phase ends
    U_FP a_rate;        // attack rate
    U_FP d_rate;        // decay rate
    U_FP sustain;       // when value <= sustain, decay phase ends
    U_FP r_rate;        // upon release decrase with r_rate until 0

    uint8_t delay_time; // time before attack
    uint8_t velocity;

    ADSR adsr;

    LFO * lfo;

    Envelope() : state(S_OFF), output(0), current(0), lfo(0)
    {}

    void init(const ADSR & _adsr)
    {
        adsr = _adsr;
        state = S_OFF;
        output = 0;
        current = 0;
    }

    void set_adsr(const ADSR & _adsr)
    {
        adsr = _adsr;
        if (state != S_OFF) {
            State saved = state;
            begin(velocity);
            //current = sustain;
            state = saved;
            switch (state) {
                case S_DELAY:   // do nothing
                    break;
                case S_ATTACK:  // ensure range
                    if (current >= (a_top <<8)) current = a_top;
                    break;
                case S_DECAY:
                    if (current < sustain) current = sustain;
                    break;
                case S_SUSTAIN:
                    current = sustain << 8;
                    break;
                case S_RELEASE:
                    break;
            }
        }
    }

    void begin(uint8_t velocity, int16_t initial = -1)
    {
        this->velocity = velocity;

        // top A value from amount and velocity
        uint32_t tmp = adsr.amount;
        if (tmp == 0) {
            current = 0;
            output = 0;
            return;
        }

        tmp *= (256 - adsr.tracking);
        a_top = tmp >> 8;

        tmp = adsr.tracking + 1;
        tmp *= velocity + 1;
        uint16_t q = tmp >> 8;

        a_top += (adsr.amount * q) >> 8;

        // scaled sustain value (also velocity dependent)
        sustain = ((a_top + 1) * adsr.s) >> 8;

        // attack rate
        a_rate = (256UL * a_top)/(adsr.a + 1);

        // decay rate: a_top down to sustain
        d_rate = 256UL * (a_top - sustain) / (adsr.d + 1);

        // release rate: sustain to zero
        r_rate = 256UL * sustain / (adsr.r + 1);

#if defined(UNITY) && TRACE_ENV
        printf("a_top=%d sustain=%d a_rate=%d.%d "
                "d_rate=%d.%d r_rate=%d.%d\n",
                a_top, sustain,
                FP_INT(a_rate), FP_DECIMAL(a_rate),
                FP_INT(d_rate), FP_DECIMAL(d_rate),
                FP_INT(r_rate), FP_DECIMAL(r_rate));
#endif

        delay_time = adsr.delay;
        state = delay_time ? S_DELAY : S_ATTACK;
        if (initial != -1) {
            current = ((uint8_t)initial) << 8;
        }
    }

    void release(uint8_t velocity)
    {
        if (current >= sustain << 8) {
            current = sustain << 8;
        }
        state = S_RELEASE;
    }

    bool update_tick()
    {
        if (lfo) lfo->update_tick();

        int16_t prev = output;

        switch (state) {
            case S_DELAY:
                if (--delay_time == 0) {
                    state = S_ATTACK;
                }
                break;
            case S_ATTACK:
                if (a_rate <= (a_top << 8) - current) {
                    current += a_rate;
                }
                else {
                    current = a_top << 8;
                }

                if (current >= a_top << 8) {
                    current = a_top << 8;
                    state = S_DECAY;
                }
                break;
            case S_DECAY:
                //if (current - d_rate >= sustain << 8) {
                if (current > d_rate) {
                    current -= d_rate;
                }
                else {
                    current = sustain << 8;
                    state = S_SUSTAIN;
                }

                if (current <= sustain << 8) {
                    current = sustain << 8;
                    state = S_SUSTAIN;
                }
                break;
            case S_SUSTAIN:
                break;
            case S_RELEASE:
                if (current >= r_rate) {
                    current -= r_rate;
                }
                else {
                    current = 0;
                }

                if (current == 0) {
                    state = S_RELEASE2;
                    //Serial.println("S_RELEASE->OFF");
                }
                break;
            case S_RELEASE2:
                state = S_OFF;
                break;
            case S_OFF:
                break;
        }
#if defined(UNITY) && TRACE_ENV
        printf("%s: state=%d cur=%d.%d\n", __FUNCTION__,
                state, FP_INT(current), FP_FRAC(current));
#endif

        output = FP_INT(current);
        if (adsr.sign < 0) output = -output;
        if (lfo) output += lfo->output;

        return prev != output;
    }

    bool is_off() const {
        return state == S_OFF;
    }
};

// instrument parameters
struct Instrument {
    Waveform    wf;             // NRPN 0: waveform
                                //      1: ringmod (0/1)
                                //      2: noise (0/1)
                                //      3: filter 0=LP, 1=BP
    ADSR        volume;         // NRPN 0x10-0x16 dly, ADSR, amnt, velo, sign
    ADSR        cutoff;         // NRPN 0x20-0x26
    ADSR        resonance;      // NRPN 0x30-0x36
    ADSR        pulse;          // NRPN 0x40-0x46
    ADSR        pitch;          // NRPN 0x50-0x56
    LFOParams   lfo;            // NRPN 0x60-0x65
    Filter      filter_band;    // FILTER_LOWPASS, FILTER_BANDPASS
    uint8_t     filter_tracking;// pitch->filter cutoff tracking

    // initial values
    uint8_t     cutoff0;        // cutoff
    uint8_t     resonance0;     // resonance
    uint8_t     pw0;            // pulse width: 128 = 50%
    int8_t      pitch0;         // pitch offset: +/- 100
    uint8_t     pitch_scale;    // << envelope << pitch_scale
    uint8_t     porta_rate;     // portamento rate: 0=off, 1=slow, 255 = fast

    LFO_Flags   lfo_flags;      // retrigger, on, target

    uint8_t     nrpn_lsb;
    uint8_t     data_msb;

    voiceflags  changed;

    Instrument() :
        wf(WF_SAW),
        volume((ADSR){0, 10, 60, 120, 10, 255, 255}), // dly,A,D,S,R,amnt,velo
        cutoff((ADSR){0, 110, 40, 140, 3, 40, 80, -1}),
        resonance((ADSR){0, 255, 28, 0, 0, 255, 255, +1}),
        pulse((ADSR){0, 0, 0, 0, 0, 255, 255}),
        pitch((ADSR){10, 20, 20, 0, 0, 20, 128, +1}),
        lfo((LFOParams){0, LFO_TRIANGLE, 10, 25, 0, 0}),
        filter_band(FILTER_LOWPASS), filter_tracking(0),
        cutoff0(140), resonance0(0), pw0(128), pitch_scale(0),
        lfo_flags(LFO_OFF), changed(0)
    { }

    void set_changed(voiceflags f) { changed = f; }

    // params with small value range are sent as single byte, but it's msb
    // data entry cc 6
    bool is_cursed_param(uint8_t param) const
    {
        static const uint8_t cursed[] = {0, 1, 2, 3, 8, 9, 10, 11,
            39, 55, 71, 87, 97 /* lfo shape */};

        for (uint8_t i = 0; i < sizeof(cursed)/sizeof(cursed[0]); ++i) {
            if (param == cursed[i]) return true;
        }

        return false;
    }

    // edit the instrument using midi NRPN
    // 98: NRPN LSB
    // 99: NRPN MSB
    // 6:  Data entry MSB
    // 38: Data entry LSB
    void midi_cc(uint8_t control, uint8_t value)
    {
        changed = 0;
        switch (control) {
            case 98: nrpn_lsb = value;  // NRPN LSB
                     break;
            case 99: break;             // NRPN MSB (ignore)
            case 6:  // data entry MSB
                     data_msb = value;
                     if (is_cursed_param(nrpn_lsb)) {
                         set_param(nrpn_lsb >> 4, nrpn_lsb & 15,
                             data_msb);
                     }
                     break;
            case 38: // data entry LSB
                     set_param(nrpn_lsb >> 4, nrpn_lsb & 15,
                             (data_msb << 7) | value);
                     break;
            default:
                     break;
        }
    }

    void set_nrpn0(uint8_t index, uint16_t value) {
        switch (index) {
            case 0: // NRPN 0: waveform
                wf = (Waveform)((wf & ~WF_MASK) | (value & WF_MASK));
                changed |= F_WF_CHANGED;
                break;
            case 1: // NRPN 1: ringmod
                wf = (Waveform)((wf & ~WF_RINGMOD) | (value ? WF_RINGMOD : 0));
                changed |= F_WF_CHANGED;
                break;
            case 2: // NRPN 2: noise
                wf = (Waveform)((wf & ~WF_NOISE) | (value ? WF_NOISE : 0));
                changed |= F_WF_CHANGED;
                break;
            case 3: // NRPN 3: filter band
                filter_band = value ? FILTER_BANDPASS : FILTER_LOWPASS;
                changed |= F_FILTER_CHANGED;
                break;
            case 4: // NRPN 4: filter cutoff
                cutoff0 = clamp8u(value);
                changed |= F_FCUT_CHANGED;
                break;
            case 5: // NRPN 5: filter resonance
                resonance0 = clamp8u(value);
                changed |= F_FRES_CHANGED;
                break;
            case 6: // NRPN 6: pw0
                pw0 = clamp8u(value);
                changed |= F_PW_CHANGED;
                break;
            case 7: // NRPN 7: pitch0: 0 = -100, 100 = 0, 200 = +100
                pitch0 = clamp16i(value - 100, -100, 100);
                changed |= F_PITCH_CHANGED;
                break;
            case 8:  // NRPN 8: keyboard filter tracking
                filter_tracking = clamp8u(value);
#if CHATTY_PARAMS
                Serial.printf("filter kbd tracking=%d\n", value);
#endif
                changed |= F_FCUT_CHANGED;
                break;
            case 9:  // NRPN 9: pitch scale 0..4
                pitch_scale = clamp16i(value, 0, 4);
                changed |= F_PITCH_CHANGED;
                break;
            case 10: // NRPN 10: LFO target
                lfo_flags = (LFO_Flags)((lfo_flags & ~LFO_TARGET_MASK) |
                    (value & LFO_TARGET_MASK));
                changed |= F_LFO_CHANGED;
                break;
            case 11: // NRPN 11: LFO retrigger
                lfo_flags = (LFO_Flags) ((lfo_flags & ~LFO_RETRIGGER) |
                    (value ? LFO_RETRIGGER : 0));
                changed |= F_LFO_CHANGED;
                break;
            case 12: // NRPN 12: portamento: 0=off, 1=slowest, 255=fastest
                porta_rate = clamp8u(value);
                changed |= F_PITCH_CHANGED;
                break;
        }
    }

    void set_param(uint8_t block, uint8_t index, uint16_t value)
    {
#if CHATTY_SET_PARAM
        Serial.printf("set_param: %d/%d=%d\n",
                block, index, value);
#endif
        switch (block) {
            case 0:
                set_nrpn0(index, value);
#if CHATTY_PARAMS
                Serial.print(F("waveform="));
                Serial.println(waveform_fstr(wf));
#endif
                break;
            case 1:
#if CHATTY_PARAMS
                Serial.print(F("volume "));
#endif
                if (!volume.set_param(index, value)) {
#if CHATTY_PARAMS
                    Serial.println(F("ERROR"));
#endif
                }
                changed |= F_VOL_CHANGED;
                break;
            case 2:
#if CHATTY_PARAMS
                Serial.print(F("fcut "));
#endif
                if (!cutoff.set_param(index, value)) {
#if CHATTY_PARAMS
                    Serial.println(F("ERROR"));
#endif
                }
                changed |= F_FCUT_CHANGED;
                break;
            case 3:
#if CHATTY_PARAMS
                Serial.print(F("fres "));
#endif
                if (!resonance.set_param(index, value)) {
#if CHATTY_PARAMS
                    Serial.println(F("ERROR"));
#endif
                }
                changed |= F_FRES_CHANGED;
                break;
            case 4:
#if CHATTY_PARAMS
                Serial.print(F("pw "));
#endif
                if (!pulse.set_param(index, value)) {
#if CHATTY_PARAMS
                    Serial.println(F("ERROR"));
#endif
                }
                changed |= F_PW_CHANGED;
                break;
            case 5:
#if CHATTY_PARAMS
                Serial.print(F("pitch "));
#endif
                if (!pitch.set_param(index, value)) {
#if CHATTY_PARAMS
                    Serial.println(F("ERROR"));
#endif
                }
                changed |= F_PITCH_CHANGED;
                break;

            case 6:
#if CHATTY_PARAMS
                Serial.print(F("lfo "));
#endif
                lfo.set_param(index, value);
                changed |= F_LFO_CHANGED;
                break;
        }
    }

    uint8_t lfo_target() const {
        return lfo_flags & LFO_TARGET_MASK;
    }
};

// voice tracks in runtime
struct Voice {
    voiceflags flags;
    uint8_t wf;
    uint8_t pw;             // result pw
    uint8_t cutoff;         // result cutoff
    uint8_t resonance;      // result resonance
    int16_t pitch0;         // note * N_CENTS
    int16_t pitch1;         // pitch bend offset
    int16_t final_pitch;    // final calculated pitch
    int16_t goal_pitch;     // portamento goal
    int16_t delta_pitch;    // add to pitch0 until == goal_pitch

    Envelope env_volume;    // volume
    Envelope env_cutoff;    // filter cutoff
    Envelope env_resonance; // filter resonance
    Envelope env_pulse;     // pwm
    Envelope env_pitch;     // pitch
    LFO     lfo;

    OnNote_Q on_note;
    int16_t note_filter_tracking;

    Instrument * instr;

    Voice()
        : flags(0),
          wf(WF_SAW), pw(128), cutoff(255), resonance(0),
          pitch0(0), pitch1(0), final_pitch(0), goal_pitch(0), delta_pitch(0),
          note_filter_tracking(0), instr(0)
    {}

    void set_instrument(Instrument * _instr)
    {
        instr = _instr;
        instr->set_changed(0xffff);
        update_from_instrument();
    }

    void update_note_filter_tracking()
    {
        // approx 10 / octave
        note_filter_tracking = clamp8u((int16_t)on_note - 24);
        note_filter_tracking = on_note * instr->filter_tracking / 12;
    }

    void update_from_instrument()
    {
        flags |= instr->changed;
        if (flags & F_WF_CHANGED) {
            wf = instr->wf;
        }
        if (flags & F_VOL_CHANGED) {
            env_volume.set_adsr(instr->volume);
        }
        if (flags & F_FCUT_CHANGED) {
            env_cutoff.set_adsr(instr->cutoff);
            update_note_filter_tracking();
        }
        if (flags & F_FRES_CHANGED) {
            env_resonance.set_adsr(instr->resonance);
        }
        if (flags & F_PW_CHANGED) {
            env_pulse.set_adsr(instr->pulse);
        }
        if (flags & F_PITCH_CHANGED) {
            env_pitch.set_adsr(instr->pitch);
        }
        if (flags & F_LFO_CHANGED) {
            lfo.init(instr->lfo);

            env_volume.lfo = env_cutoff.lfo = env_resonance.lfo =
                env_pulse.lfo = env_pitch.lfo = 0;

            switch (instr->lfo_flags & LFO_TARGET_MASK) {
                case LFO_PITCH: env_pitch.lfo = &lfo; break;
                case LFO_CUTOFF: env_cutoff.lfo = &lfo; break;
                case LFO_RESONANCE: env_resonance.lfo = &lfo; break;
                case LFO_PULSE: env_pulse.lfo = &lfo; break;
                case LFO_VOL: env_volume.lfo = &lfo; break;
                case LFO_WAVEFORM: // todo
                              break;
                default:
                              break;
            }
            //Serial.printf("lfo pitch %d cut %d res %d pulse %d vol %d "
            //        " shape=%d\n",
            //        env_pitch.lfo, env_cutoff.lfo, env_resonance.lfo,
            //        env_pulse.lfo, env_volume.lfo, lfo.shape);
        }
    }

    // default range is +-8192 -> +-2 semitones = +- 200 cents
    void pitch_bend(int pitch)
    {
        int32_t p = 200L * pitch;
        pitch1 = p >> 13;
        flags |= F_PITCH_CHANGED;
        //Serial.printf("bend=%d pitch1=%d\n", pitch, pitch1);
    }

    void note_on(uint8_t note, uint8_t velocity)
    {
        if (!instr) {
            // somehow sometimes there comes a note_on when just plugged in
            return;
        }
        uint8_t prev_note = on_note;
        on_note.key_down(note);

        if (prev_note == on_note) {
            return;
        }

        int16_t new_pitch = on_note * N_CENTS;
        if (instr->porta_rate == 0 || prev_note == 0) {
            pitch0 = final_pitch = new_pitch;
            delta_pitch = 0;
        }
        else {
            goal_pitch = new_pitch;
            delta_pitch = instr->porta_rate;
            if (goal_pitch < pitch0) {
                delta_pitch = -delta_pitch;
            }
        }
        flags |= F_PITCH_CHANGED;

        //if (new_pitch != final_pitch) {
        //    pitch0 = final_pitch = new_pitch;
        //    flags |= F_PITCH_CHANGED;
        //}

        env_volume.begin(velocity);
        env_cutoff.begin(velocity, 0);
        env_resonance.begin(velocity, 0);
        env_pulse.begin(velocity, 0);
        env_pitch.begin(velocity, 0);
        lfo.begin(velocity);

        flags |= F_NOTE_STARTED | F_FCUT_CHANGED | F_FRES_CHANGED |
            F_PW_CHANGED;

        update_note_filter_tracking();
    }

    void note_off(uint8_t note, uint8_t velocity)
    {
        on_note.key_up(note);
        if (on_note == 0) {
            // all notes off
            env_volume.release(velocity);
            env_cutoff.release(velocity);
            env_resonance.release(velocity);
            env_pulse.release(velocity);
            env_pitch.release(velocity);
            lfo.release(velocity);
        }
        else {
            // return to the lower note
            int16_t new_pitch = on_note * N_CENTS;
            pitch0 = final_pitch = new_pitch;
            flags |= F_PITCH_CHANGED;
        }
    }

    void porta_tick() {
        if (delta_pitch > 0) {
            pitch0 += delta_pitch;
            if (pitch0 >= goal_pitch) {
                pitch0 = goal_pitch;
                delta_pitch = 0;
            }
            flags |= F_PITCH_CHANGED;
        }
        else if (delta_pitch < 0) {
            pitch0 += delta_pitch;
            if (pitch0 <= goal_pitch) {
                pitch0 = goal_pitch;
                delta_pitch = 0;
            }
            flags |= F_PITCH_CHANGED;
        }
    }

    // Voice::
    void update_tick() {
        if (env_volume.is_off()) {
            return;
        }

        porta_tick();

        lfo.update_tick();

        if (env_volume.update_tick()) {
            flags |= F_VOL_CHANGED;
        }

        flags |= env_cutoff.update_tick() ? F_FCUT_CHANGED : 0;
        if (flags & F_FCUT_CHANGED) {
            cutoff = clamp8u((int16_t)instr->cutoff0 + note_filter_tracking + env_cutoff.output);
            //cutoff = clamp8u((int16_t)instr->cutoff0 + env_cutoff.output);
        }

        flags |= env_resonance.update_tick() ? F_FRES_CHANGED : 0;
        if (flags & F_FRES_CHANGED) {
            resonance = clamp8u((int16_t)instr->resonance0 + env_resonance.output);
        }

        flags |= env_pulse.update_tick() ? F_PW_CHANGED : 0;
        if (flags & F_PW_CHANGED) {
            pw = clamp8u((int16_t)instr->pw0 + env_pulse.output);
        }

        flags |= env_pitch.update_tick() ? F_PITCH_CHANGED : 0;
        if (flags & F_PITCH_CHANGED) {
            final_pitch = pitch0 + pitch1 + instr->pitch0 + (env_pitch.output << instr->pitch_scale);
        }
        //Serial.printf("update_tick() flags=%04x\n", flags);
    }
};

static bool is_wf_off(Waveform wf)
{
    //Serial.printf("is_wf_off: wf=%02x result=%d\n",
    //        wf, (WF_MASK & wf) != WF_OFF);
    return (WF_MASK & wf) == WF_OFF;
}

enum chorusflg {
    CHORUS_LEVEL = 1,
    CHORUS_RATE = 2
};

struct Synth {
    Instrument instr[N_INSTRUMENTS];
    Voice voice[N_CHANNELS];        // starts with 0 (midi starts wih 1)
    uint8_t instr_map[16];          // instruments in all channels (for configuration)

    Petomane & petomane;
    Tuning & tuning;
    uint8_t chan_enabled;           // channel enabled flags bitmap
    uint8_t chorus_level, chorus_rate;
    uint8_t chorus_flags;
    LFOParams chorus_lfo_params;
    LFO chorus_lfo;

    Synth(Petomane & petomane, Tuning & tuning)
        : petomane(petomane), tuning(tuning), chan_enabled(0),
        chorus_level(0), chorus_rate(0), chorus_flags(255),
        chorus_lfo_params({0, LFO_TRIANGLE, 0, 0, 0, 0, 0})
    {
        chorus_lfo.init(chorus_lfo_params);

        // to print sizeof at compile-time
        //(char[sizeof(Instrument)])"bla";  -- 64
        //(char[sizeof(Voice)])"bla";       -- 171
    }

    void init()
    {
        for (int i = 0; i < 16; ++i) {
            program_change(i, i);
        }
    }

    void program_change(uint8_t channel, uint8_t program)
    {
        if (channel >= 16 || program >= N_INSTRUMENTS) return;

        instr_map[channel] = program;
        if (channel < N_CHANNELS && program < N_INSTRUMENTS) {
            voice[channel].set_instrument(&instr[program]);
            hw_all_params(channel, instr[program]);
        }
    }

    void midi_cc(uint8_t channel, uint8_t cc, uint8_t value)
    {
        if (channel > 16) {
            return;
        }

        // control chorus in channel 5
        if (channel == 5) {
            switch (cc) {
                case 91:    // CC91 chorus rate/delay time
                    chorus_rate = value << 1;
                    chorus_flags |= CHORUS_RATE;
                    return;
                case 93:   // CC93 chorus mix level
                    chorus_level = value << 1;
                    chorus_flags |= CHORUS_LEVEL;
                    return;
                case 94:   // CC94 chorus lfo amount -64..63 (0=64)
                    chorus_lfo.amount = (((uint16_t)value) - 64) << 2;
                    return;
                case 95:    // CC95 chorus lfo speed 0..127
                    chorus_lfo.delta = (uint32_t)value << (3 + 15);
                    return;
            }
        }

        instr[instr_map[channel]].midi_cc(cc, value);
        if (channel < N_CHANNELS) {
            //voice[channel].instr->midi_cc(cc, value);
            voice[channel].update_from_instrument();
        }
    }

    void hw_all_params(uint8_t channel, Instrument& ins)
    {
        // sound off
        petomane.cv_env(channel, 0);
        enable_chan(channel, false);

        // set non-realtime hw parameters
        petomane.set_filter_band(channel, ins.filter_band);

        // set initial parameters
        // waveform, ringmod, noise
        petomane.set_waveform(channel, ins.wf);
        // pulse width
        petomane.cv_pwm(channel, ins.pw0);
        // filter cutoff, resonance
        //petomane.cv_cutoff(channel, ins.cutoff0);
        //petomane.cv_resonance(channel, ins.resonance0);
    }

    // return true if there was change
    bool enable_chan(Petomane::Channel chan, bool enable)
    {
        if (enable) {
            //if (!(chan_enabled & (1 << chan))) {
                petomane.set_enabled(chan, !is_wf_off((Waveform)voice[chan].wf));
                chan_enabled |= 1 << chan;
                return true;
            //}
        }
        else {
            //if (chan_enabled & (1 << chan)) {
                petomane.set_enabled(chan, false);
                chan_enabled &= ~(1 << chan);
                return true;
            //}
        }
        return false;
    }

    void set_freq(Petomane::Channel chan, uint16_t pitch, uint8_t wf)
    {
        if (is_wf_off((Waveform)wf)) return;

        uint16_t divider = tuning.get_divider(pitch);
        // 2x frequency for triangle in channels 1, 3, 5
        wf = (Waveform)(wf & WF_MASK);
        if (wf == WF_TRIANGLE && (chan & 1)) {
            divider >>= 1;
        }
        petomane.set_div(chan, divider);
    }

    // Synth::
    void update_tick() {
        for (Petomane::Channel chan = 0; chan < N_CHANNELS; ++chan) {
        //for (Petomane::Channel chan = 4; chan < 5; ++chan) {
            voiceflags & flg = voice[chan].flags;

            if (flg & F_NOTE_STARTED) {
                if (enable_chan(chan, true)) {
                    flg |= F_PITCH_CHANGED;
                }
            }

            voice[chan].update_tick();

            uint8_t vol = voice[chan].env_volume.output;
            petomane.cv_env(chan, vol);
            if (flg & F_VOL_CHANGED) {
                if (vol == 0) {
                    enable_chan(chan, false);
                }
            }
            if (flg & F_WF_CHANGED) {
                petomane.set_waveform(chan, (Waveform)voice[chan].wf);
                if (voice[chan].env_volume.output > 0) {
                    enable_chan(chan, true);
                    set_freq(chan, voice[chan].final_pitch, voice[chan].wf); //?
                }
            }
            if (flg & F_FILTER_CHANGED) {
                petomane.set_filter_band(chan, voice[chan].instr->filter_band);
            }
            //if (flg & F_FCUT_CHANGED) {
                petomane.cv_cutoff(chan, voice[chan].cutoff);
            //    Serial.printf("fcut=%d\n", voice[chan].cutoff);
            //}
            //if (flg & F_FRES_CHANGED) {
                petomane.cv_resonance(chan, voice[chan].resonance);
            //}
            if (flg & F_PW_CHANGED) {
                petomane.cv_pwm(chan, voice[chan].pw);
            }
            if (flg & F_PITCH_CHANGED) {
                if (!is_wf_off((Waveform)voice[chan].wf)) {
                    set_freq(chan, voice[chan].final_pitch, voice[chan].wf);
                }
            }

            flg = 0;
        }

        if (chorus_lfo.update_tick() || (chorus_flags & CHORUS_RATE)) {
            petomane.cv_chorus_rate(
                    clamp8u((int16_t)chorus_rate + chorus_lfo.output));
            //Serial.printf("chrate=%d\n", clamp8u((int16_t)chorus_rate + chorus_lfo.output));
        }
        if (chorus_flags & CHORUS_LEVEL) {
            petomane.cv_chorus_level(chorus_level);
        }
        chorus_flags = 0;
    }

    void note_on(uint8_t chan, uint8_t note, uint8_t velocity)
    {
        if (chan < N_CHANNELS) {
            voice[chan].note_on(note, velocity);
        }
    }

    void note_off(uint8_t chan, uint8_t note, uint8_t velocity)
    {
        if (chan < N_CHANNELS) {
            voice[chan].note_off(note, velocity);
        }
    }

    void pitch_bend(uint8_t chan, int pitch)
    {
        if (chan < N_CHANNELS) {
            voice[chan].pitch_bend(pitch);
        }
    }
};
