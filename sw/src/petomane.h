#pragma once

#include <stdint.h>

#include "synth_types.h"

struct register_bank
{
    uint8_t r[4];
};

struct register_state
{
    uint8_t pa;
    uint8_t pb;     
    uint8_t pc;

    register_bank shadow[4];
};

class Petomane
{
    register_state _regs;

public:
    typedef uint8_t Channel;

    enum Error {
        ERR_OK = 0,
        ERR_WAVEFORM = -1,
        ERR_RINGMOD_CHAN = -2,  // no ringmod on this channel
        ERR_CHAN = -3,          // incorrect channel
        ERR_AMUX_CHAN = -4,     // incorrect AMUX channel
        ERR_PWM_CHAN = -5,      // PWM on unsupported channel
    };

    enum Device {
        DEV_PPI = 0,
        DEV_CTR0 = 2,
        DEV_CTR1 = 1
    };
    
    enum PPI_Reg {
        PPI_A = 0, PPI_B, PPI_C, PPI_CW
    };

    enum CTR_Reg {
        CTR0 = 0, CTR1, CTR2, CTR_CW
    };

    enum PU_PORT {
        PU_A = 0, PU_B = 1, PU_C = 2
    };

    // set s&h value on cvn to value
    void set_cv(uint8_t cvn, uint8_t value);

    void bsr_dev(uint8_t device, uint8_t reg, uint8_t mask, uint8_t set);
    void bsr_pu(uint8_t port, uint8_t mask, uint8_t set);

public:
    Error error;

    Petomane();

    void board_init();
    
    // start/stop square wave generation on chan
    void set_enabled(Channel chan, bool enable);

    bool is_enabled(Channel chan) const;

    // set 16-bit counter divider
    void set_div(Channel chan, uint16_t divider);

    // select waveform
    void set_waveform(Channel chan, Waveform wf);

    // select filter band
    void set_filter_band(Channel chan, Filter b);

    // ring mod chan 1, 3, 5 with previous one
    void set_ringmod(Channel chan, bool on);

    // enable noise on chan 4, 5
    void set_noise(Channel chan, bool on);

    // set PWM cv value for chan 1, 3, 5
    void cv_pwm(Channel chan, uint8_t cmp);

    // set filter cutoff for chan
    void cv_cutoff(Channel chan, uint8_t fc);

    // set filter resonance for chan
    void cv_resonance(Channel chan, uint8_t q);

    // set envelope for chan
    void cv_env(Channel chan, uint8_t val);

    // set global delay parameters
    void cv_chorus_rate(uint8_t rate);

    void cv_chorus_level(uint8_t level);
};
