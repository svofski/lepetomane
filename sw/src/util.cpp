#include <inttypes.h>
#include <pgmspace.h>
#include <WString.h>
#include "synth_types.h"



const __FlashStringHelper * waveform_fstr(uint8_t wf)
{
    switch (wf) {
        case WF_SAW:        return F("saw");
        case WF_SQUARE:     return F("square");
        case WF_TRIANGLE:   return F("triangle");
        case WF_RINGMOD:    return F("ringmod");
    }
    return F("<unknown waveform>");
}

const __FlashStringHelper * lfo_shape_fstr(uint8_t s)
{
    switch (s) {
        case LFO_TRIANGLE:  return F("triangle");
        case LFO_SAW:       return F("saw");
        case LFO_SQUARE:    return F("square");
        case LFO_RANDOMSH:  return F("random s&h");
    }
    return F("<unknown shape>");
}

// ADSR envelope parameter names

static const char s_delay[] PROGMEM =   "delay";
static const char s_attack[] PROGMEM =  "attack";
static const char s_decay[] PROGMEM =   "decay";
static const char s_sustain[] PROGMEM = "sustain";
static const char s_release[] PROGMEM = "release";
static const char s_amount[] PROGMEM =  "amount";
static const char s_tracking[] PROGMEM = "tracking";

static const char * const adsr_param_names[] PROGMEM = 
    { s_delay, s_attack, s_decay, s_sustain, s_release, s_amount, s_tracking };

const __FlashStringHelper * adsr_param_fstr(uint8_t index)
{
    if (index >= sizeof(adsr_param_names)/sizeof(adsr_param_names[0])) {
        return F("<unknown adsr param>");
    }
    return FPSTR(pgm_read_dword(&(adsr_param_names[index])));
}

// LFO parameter names
static const char s_shape[] PROGMEM =   "shape";
static const char s_offset[] PROGMEM =  "offset";
static const char s_rate[] PROGMEM =    "rate";
static const char s_retrigger[] PROGMEM = "retrigger";
static const char s_flags[] PROGMEM =   "phase";

static const char * const lfo_param_names[] PROGMEM =
    { s_delay, s_shape, s_amount, s_offset, s_rate, s_retrigger, s_flags };

const __FlashStringHelper * lfo_param_fstr(uint8_t index)
{
    if (index >= sizeof(lfo_param_names)/sizeof(lfo_param_names[0])) {
        return F("<unknown lfo param>");
    }
    return FPSTR(pgm_read_dword(&(lfo_param_names[index])));
}
