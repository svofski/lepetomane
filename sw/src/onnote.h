#pragma once

#include <stdint.h>


// this will work for two keys but results will be funny for more than 2
struct OnNote {
    uint8_t low, high;

    OnNote(): low(0), high(0)
    {
    }

    // 1st note on: high = key, low = key
    // second note on: low...high=key
    //           or    low=key..high
    void key_down(int key)
    {
        if (key > high) {
            high = key;
            if (low == 0) {
                low = key;
            }
        }
        else if (key < low) {
            low = key;
        }
    }

    // high note off -> low..low
    // low note off  -> high..high
    void key_up(int key)
    {
        if (high == low && high == key) {
            high = low = 0;
        }
        else if (key == high) {
            high = low;
        }
        else if (key == low) {
            low = high;
        }
        // releasing any key between low and high changes nothing
    }

    operator uint8_t() const { return high; }
};

struct OnNote_Q
{
    static constexpr int N = 4;

    uint8_t q[N];

    OnNote_Q()
    {
        for (int i = 0; i < N; ++i) q[i] = 0;
    }

    void key_down(int key)
    {
        uint8_t i_min = 0, min = 127;
        for (int i = 0; i < N; ++i) {
            if (q[i] < min) {
                min = q[i];
                i_min = i;
            }
        }
        q[i_min] = key;
    }

    void key_up(int key)
    {
        for (int i = 0; i < N; ++i) {
            if (q[i] == key) {
                q[i] = 0;
                break;
            }
        }
    }

    operator uint8_t() const {
        uint8_t max = 0;
        for (int i = 0; i < N; ++i) {
            if (q[i] > max) max = q[i];
        }
        return max;
    }
};

