#include <stdint.h>
#include "tuning.h"

#ifdef ARDUINO
#include <pgmspace.h>
#endif

#ifdef NATIVE
#define PGMSPACE
uint16_t pgm_read_word(const uint16_t * p)
{
    return *p;
}
#endif

#include "note2div.h"

uint16_t Tuning::get_divider(uint8_t note, int8_t cents) const
{
    int16_t index = note * N_CENTS;
    index += cents;
    return get_divider(index);
}

uint16_t Tuning::get_divider(int16_t index) const
{
    if (index >= 0 && index < int(sizeof(note2div)/sizeof(note2div[0]))) {
        return pgm_read_word(&(note2div[index]));
    }

    return 0;
}
