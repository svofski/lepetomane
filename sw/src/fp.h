#pragma once

// all numbers are 8.8 fixed point
typedef uint16_t U_FP;

#define FP_INT(x) ((x)>>8)
#define FP_FRAC(x) ((x) & 255)
#define FP_DECIMAL(x) ((uint16_t)((((uint32_t)FP_FRAC(x)) * 1000UL) >> 8))

#define FP_MAKE(i,f) (((i)<<8) | (((unsigned long)(f))*256UL)/1000UL)
