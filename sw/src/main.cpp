#include <Arduino.h>

#include "synth_types.h"
#include "synth.h"
#include "petomane.h"

#define WITH_SYNTH 1
#define TEST_ROLLING_CVS 0
#define TEST_NOTES 0
#define CONTINUOUS_TEST 0

#define LED_HEARTBEAT 1
#define LED_PIN 6

#define PRINT_MIDI_MESSAGES 0

Petomane petomane;
Tuning tuning;
Synth synth(petomane, tuning);

int note_ons = 0;

uint32_t last_millis;

#ifdef USB_MIDI
void myNoteOn(byte channel, byte note, byte velocity) {
    // When using MIDIx4 or MIDIx16, usbMIDI.getCable() can be used
    // to read which of the virtual MIDI cables received this message.
    --channel;
#if PRINT_MIDI_MESSAGES
    Serial.print("Note On, ch=");
    Serial.print(channel, DEC);
    Serial.print(", note=");
    Serial.print(note, DEC);
    Serial.print(", velocity=");
    Serial.println(velocity, DEC);
#endif

    synth.note_on(channel, note, velocity << 1);
}

void myNoteOff(byte channel, byte note, byte velocity) {
    --channel;
#if PRINT_MIDI_MESSAGES
    Serial.print("Note Off, ch=");
    Serial.print(channel, DEC);
    Serial.print(", note=");
    Serial.print(note, DEC);
    Serial.print(", velocity=");
    Serial.println(velocity, DEC);
#endif

    synth.note_off(channel, note, velocity << 1);
}
// NRPN: CC 0x62 NRPN LSB, 0x63 NRPN MSB
// 6, 38, 96, and 97 data entry
void myControlChange(byte channel, byte control, byte value) {
    --channel;

#if PRINT_MIDI_MESSAGES
    Serial.print("Control Change, ch=");
    Serial.print(channel, DEC);
    Serial.print(", control=");
    Serial.print(control, DEC);
    Serial.print(", value=");
    Serial.println(value, DEC);
#endif

    synth.midi_cc(channel, control, value);
}

void myProgramChange(byte channel, byte program)
{
    --channel;

#if PRINT_MIDI_MESSAGES
    Serial.printf("Program Change, ch=%d prog=%d\n", channel, program);
#endif
    synth.program_change(channel, program);
}

void myPitchChange(byte chan, int pitch)
{
    --chan;
#if PRINT_MIDI_MESSAGES
    Serial.printf("Pitch Bend, ch=%d bend=%d\n", chan, pitch);
#endif
    synth.pitch_bend(chan, pitch - 8192);
}

void mySystemExclusiveChunk(const byte *data, uint16_t length, bool last)
{
    Serial.printf("SysEx chunk len=%d last=%d\n", length, last);
    for (uint16_t i = 0; i < length; ++i) {
        Serial.printf("%02x ", data[i]);
    }
    Serial.println();
    static const byte id[] {0xf0, 0x7e, 0x7f, 0x06, 0x01, 0xf7};
    static const byte id_ack[] {
        0xF0, 0x7E, /* device id */ 0xb0, 0x06, 0x02,
            /* mfg sysex id */ 0x01,
            /* device family code */ 0x01, 0x02,
            /* device family member code */ 0x03, 0x04,
            /* software revision */ 0x01, 0x02, 0x03, 0x04,
            0xF7
    };
    static byte idx = 0;
    for (uint16_t i = 0; i < length && idx < sizeof(id); ++i) {
        if (data[i] == id[idx]) {
            if (++idx == sizeof(id)) {
                idx = 0;
                Serial.printf("Universal ID\n");
                usbMIDI.sendSysEx(sizeof(id_ack), id_ack, true);
            }
        }
        else {
            idx = 0;
        }
    }
    Serial.println("SysEx done");
}

void midi_setup()
{
    usbMIDI.setHandleNoteOn(myNoteOn);
    usbMIDI.setHandleNoteOff(myNoteOff);
    usbMIDI.setHandleControlChange(myControlChange);
    usbMIDI.setHandleProgramChange(myProgramChange);
    usbMIDI.setHandlePitchChange(myPitchChange);
    usbMIDI.setHandleSystemExclusive(mySystemExclusiveChunk);
}

void midi_loop()
{
    // The handler functions are called when usbMIDI reads data.  They
    // will not be called automatically.  You must call usbMIDI.read()
    // regularly from loop() for usbMIDI to actually read incoming
    // data and run the handler functions as messages arrive.
    usbMIDI.read();
}
#endif

void setup() {
    Serial.begin(115200);
    Serial.println("Le Petomane");
    pinMode(LED_PIN, OUTPUT);

#ifdef USB_MIDI
    midi_setup();
#endif

    petomane.board_init();

#if WITH_SYNTH
    synth.init();
#endif

    last_millis = millis();
}

#if LED_HEARTBEAT
int cnt = 0;
int leds = 0;
#endif

int idle_min = 0;
int idle_now = 0;
int idle_max = 0;
int idle_count = 0;

#if TEST_ROLLING_CVS

// Test analog multiplexing:
// Update all available voltages, with as large jumps between neighbouring
// values as possible.
// On the scope, observe that the CVs track linearly without crosstalk.
void rolling_cvs()
{
    static uint8_t cv0 = 0;
    static uint8_t cv1 = 0;

    static uint8_t chan = 0;

    cv0 += 3;
    --cv1;

    //for (uint8_t i = 0; i < 12; ++i) {
    //    petomane.set_cv(0 + (i << 1), cv0);
    //    petomane.set_cv(1 + (i << 1), cv1);
    //}
    if (Serial.available()) {
        while (Serial.available()) Serial.read();
        ++chan;
        if (chan == 24) {
            chan = 0;
        }
        Serial.printf("chan=%d\n", chan);
    }
    petomane.set_cv(chan, cv0);
}
#endif

#if TEST_NOTES
void send_nrpn(Synth& synth, uint8_t chan, int index, int data)
{
    synth.midi_cc(chan, 0x63, (index >> 7) & 0x7f); // NRPN coarse
    synth.midi_cc(chan, 0x62, index & 0x7f);        // NRPN fine
    synth.midi_cc(chan, 0x06, (data >> 7) & 0x7f);  // Data Entry coarse
    synth.midi_cc(chan, 0x26, data & 0x7f);         // Data Entry fine
}

void rolling_notes()
{
    static uint8_t nseq[] = {0, 4, 7, /*12, */ 16, /* 19, 24, 28*/};
    static uint8_t bseq[] = {20, 27, 25, 27};

    static uint8_t note = 0;
    static uint8_t base = 0;

    static uint8_t chan = 0;

    static Waveform wfseq[] = {WF_SAW, WF_SQUARE};
    static uint8_t wfseq_i = 0;

    static uint8_t volseq[] = {128, 192, 250};
    static uint8_t volseq_i = 0;

    static uint8_t fcseq[] = {64, 96, 128, 160, 192};
    static uint8_t fcseq_i = 0;

    static uint8_t fqseq[] = {0, 32, 64, 96, 128, 160, 192, 224, 255};
    static uint8_t fqseq_i = 0;

    synth.voice[4].note_off(0, 255);
    //if (note + 5 == 40) {
    //    return;
    //}
    //note = (note + 5) % 40;
    note = (note + 1) % (sizeof(nseq)/sizeof(nseq[0]));
    if (note == 0) {
        base = (base + 1) % (sizeof(bseq)/sizeof(bseq[0]));
    }

    if (1||note == 0) {
        // edit instrument
        wfseq_i = (wfseq_i + 1) % (sizeof(wfseq)/sizeof(wfseq[0]));
        volseq_i = (volseq_i + 1) % (sizeof(volseq)/sizeof(volseq[0]));
        fcseq_i = (fcseq_i + 1) % (sizeof(fcseq)/sizeof(fcseq[0]));
        fqseq_i = (fqseq_i + 1) % (sizeof(fqseq)/sizeof(fqseq[0]));
        send_nrpn(synth, 4, 0x00 /* waveform */, wfseq[wfseq_i]);
        send_nrpn(synth, 4, 0x15 /* volume env amount */, volseq[volseq_i]);
        send_nrpn(synth, 4, 0x25 /* fc amount */, fcseq[fcseq_i]);
        send_nrpn(synth, 4, 0x35 /* fq amount */, fqseq[fqseq_i]);
    }

    synth.voice[4].note_on(bseq[base] + nseq[note], 200);

    //    Serial.printf("note_off ch=%d %d\n", chan, note);
    //    synth.voice[chan].note_off(note, 0);
    //    note = (note + 1) % 108;
    //    chan = note % 6;
    //    //chan = note & 1;
    //    synth.voice[chan].note_on(note, 255);
    //    Serial.printf("note_on  ch=%d %d\n", chan, note);
}
#endif

uint16_t tix_vol = 255<<2;
uint16_t tix_fc = 0;
uint16_t tix_fq = 0;

int first_time = 0;

void loop() {
    ++idle_count;

#ifdef USB_MIDI
    midi_loop();
#endif


#if CONTINUOUS_TEST
    if (first_time == 0) {
        first_time = 1;
        synth.voice[4].note_on(38, 255);
    }
#endif

    uint32_t now = millis();
    uint32_t diff = now - last_millis;
    if (diff > INTERVAL_MS) {
        last_millis = now - (diff - INTERVAL_MS);
#if WITH_SYNTH
        synth.update_tick();

        //petomane.cv_delay(/* rate */250, /* volume */ 250);

#if CONTINUOUS_TEST
        //if (tix_vol > 0)
        tix_vol -= 1;
        tix_fc += 1;
        tix_fq += 1;

        petomane.cv_env(4, 255);//tix_vol >> 2);//tix_fc >> 2);
        petomane.cv_cutoff(4, 255);
        petomane.cv_resonance(4, tix_fq >> 2);

        //petomane.cv_resonance(4, 200);
        //petomane.cv_cutoff(4, 100);//tix_fc >> 2);
        //petomane.cv_resonance(4, tix_fq >> 2);

#endif
        //        petomane.cv_env(4, tix_vol >> 2);
        //        petomane.cv_cutoff(4, tix_fc >> 2);
        //        petomane.cv_resonance(4, tix_fq >> 2);

        //        petomane.cv_env(4, 255);//tix_fc >> 2);
        //        // 40 is smallest for kotov, 80 good for testing intermediate value
        //        petomane.cv_cutoff(4, 80);//tix_fc >> 2);
        //        petomane.cv_resonance(4, tix_fc >> 2);
#endif

#if TEST_ROLLING_CVS
        rolling_cvs();
#endif


        idle_now = idle_count;
        idle_min = min(idle_min, idle_now);
        idle_max = max(idle_max, idle_now);
        idle_count = 0;

#if LED_HEARTBEAT
        cnt += INTERVAL_MS;
        if (cnt >= 1000) {
            cnt -= 1000;
            //leds ^= 1;
            //digitalWrite(LED_PIN, leds);

#if TEST_NOTES
            rolling_notes();
#endif

            //Serial.printf("idle: %d %d %d\n", idle_now, idle_min, idle_max);
            idle_min = 32767;
            idle_max = 0;
        }
#endif
    }

}


