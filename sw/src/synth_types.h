#pragma once

#include <inttypes.h>

#ifdef ARDUINO
#include <WString.h>
#else
typedef char * __FlashStringHelper;
#endif

// main 82c54 oscillator frequency
#define FOSC_HZ         8000000L

// number of note subdivisions
#define N_CENTS         100

// number of instruments in ram (don't make it less than 16)
#define N_INSTRUMENTS   16
#define N_CHANNELS      6   // start with 0 (MIDI start with 1)

// update interval: 5ms
#define INTERVAL_MS     (1000/200)

enum Filter : uint8_t {
    FILTER_LOWPASS = 0,
    FILTER_BANDPASS
};

enum Waveform : uint8_t {
    WF_SAW = 0,         // chan 1, 2, 3, 4, 5
    WF_SQUARE = 1,      // chan 0, 2, 4 (PWM on chan 1, 3, 5)
    WF_TRIANGLE = 2,    // chan 1, 3, 5
    WF_OFF = 3,         // for ringmod and noise

    WF_LAST = WF_TRIANGLE,
    WF_MASK = 15,

    WF_RINGMOD = 16,    // chan 1x0, 3x2, 5x4: it's a flag bit
    WF_NOISE = 32,      // chan 4, 5 can add white noise
};

enum LFO_Shape : uint8_t {
    LFO_TRIANGLE = 0,
    LFO_SAW = 1,
    LFO_SQUARE = 2,
    LFO_RANDOMSH = 3,

    LFO_LAST = LFO_RANDOMSH
};

enum LFO_Flags : uint8_t {
    LFO_OFF = 0,            // not connected
    LFO_PITCH = 1,          // LFO to pitch
    LFO_CUTOFF = 2,         // LFO to filter cutoff
    LFO_RESONANCE = 3,      // LFO to filter resonance
    LFO_PULSE = 4,          // LFO to pulse width
    LFO_VOL = 5,            // LFO to volume
    LFO_WAVEFORM = 6,       // LFO to waveform
    LFO_ON = 64,            // active
    LFO_RETRIGGER = 128,    // retrigger on every begin()
    LFO_TARGET_MASK = 0x3f
};

enum State : uint8_t {
    S_OFF = 0, S_DELAY, S_ATTACK, S_DECAY, S_SUSTAIN, S_RELEASE, S_RELEASE2,
};


#ifndef FPSTR
#define FPSTR(pstr_pointer) (reinterpret_cast<const __FlashStringHelper *>(pstr_pointer))
#endif

const __FlashStringHelper * waveform_fstr(uint8_t wf);
const __FlashStringHelper * adsr_param_fstr(uint8_t index);
const __FlashStringHelper * lfo_param_fstr(uint8_t index);
const __FlashStringHelper * lfo_shape_fstr(uint8_t index);
