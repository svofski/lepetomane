#include <stdint.h>
#include "synth_types.h"

#ifdef NATIVE
#define PROGMEM
#endif

struct Tuning {
    uint16_t get_divider(uint8_t note, int8_t cents) const;
    uint16_t get_divider(int16_t index) const;
};

