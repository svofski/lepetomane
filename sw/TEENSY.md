Running
=======

```
platformio test -e hwtest
platformio test -e native
platformio run -e teensy2pp -t upload
```

Serial Monitor
==============

Teensy has a peculiar set of tools not documented anywhere.

To probe the ports:

```
~/.platformio/packages/tool-teensy/teensy_ports -L
```

To run HID serial monitor on found port:

```
~/.platformio/packages/tool-teensy/teensy_serialmon -v usb:fa130000
```

See also
========
MegaMIDI implements both MIDI and CDC Serial on Teensy++ 2.0

https://github.com/AidanHockey5/MegaMIDI/

It may or may not be worth it.


