#!/usr/bin/env python3
# This is a little script that helps relaying NRPN messages on macOS
# It should not be needed
import mido
print(f'Inputs: {mido.get_input_names()}')
print(f'Outputs: {mido.get_output_names()}')

in_name = 'R√©seau oso'
out_name = 'Titsy Petomane'

try:
    minput = mido.open_input(in_name)
except:
    print('Input "{in_name}" not available')
    exit(1)

try:
    moutput = mido.open_output(out_name)
except:
    print(f'Output "{out_name}" not available')
    exit(1)

print(f'Relaying all messages from {minput.name} to {moutput.name}')
while True:
    moutput.send(minput.receive())


